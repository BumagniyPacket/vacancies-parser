import os
import random
import re
import time

from queue import Queue

import requests

from .avito.funcs import list_creator as avito_list_creator
from .avito.funcs import parse_function as avito_parse_function
from .hh.funcs import list_creator as hh_list_creator
from .hh.funcs import parse_function as hh_parse_function
from .rabota.funcs import list_creator as rabota_list_creator
from .rabota.funcs import parse_function as rabota_parse_function
from .rdwru.funcs import list_creator as rwdru_list_creator
from .rdwru.funcs import parse_function as rwdru_parse_function
from .superjob.funcs import list_creator as superjob_list_creator
from .superjob.funcs import parse_function as superjob_parse_function
from .vars import HHRU, AVITO, RABOTA, RDWRU, SUPERJOB, TRUDVSEM, ZARPLATA
from .zarplata.funcs import list_creator as zarplata_list_creator
from .zarplata.funcs import parse_function as zarplata_parse_function


def get_resource_name_by_id(resource: int) -> str:
    func_dict = {
        HHRU: 'hh',
        AVITO: 'avito',
        RABOTA: 'rabota',
        RDWRU: 'rdwru',
        SUPERJOB: 'superjob',
        TRUDVSEM: 'trudvsem',
        ZARPLATA: 'zarplata'
    }
    return func_dict[resource]


def get_list_creation_function_by_resource(resource: int):
    """По указанному id возвращает функцию для создания списка."""
    func_dict = {
        HHRU: hh_list_creator,
        AVITO: avito_list_creator,
        RABOTA: rabota_list_creator,
        RDWRU: rwdru_list_creator,
        SUPERJOB: superjob_list_creator,
        # TRUDVSEM: trudvsem_list_creator,
        ZARPLATA: zarplata_list_creator
    }
    return func_dict[resource]


def get_parse_function_by_resource(resource: int):
    """По указанному id возвращает функцию для разбора вакансий."""
    func_dict = {
        HHRU: hh_parse_function,
        AVITO: avito_parse_function,
        RABOTA: rabota_parse_function,
        RDWRU: rwdru_parse_function,
        SUPERJOB: superjob_parse_function,
        # TRUDVSEM: trudvsem_parse_function,
        ZARPLATA: zarplata_parse_function
    }
    return func_dict[resource]


def validate_directory(directory_name: str, resource: str) -> bool:
    """Функция проверки существования директорий."""
    resource_name = get_resource_name_by_id(resource)
    path = os.path.join(directory_name, resource_name, 'img')
    print(f'Выбрана директория "{directory_name}" для сохранения результатов')
    # Есть все
    if os.path.exists(directory_name) and os.path.isdir(directory_name):
        # Есть только дирнаме
        if not os.path.exists(path):
            os.makedirs(path)
        return True
    # Нет ничего
    else:
        print(f'Директория "{directory_name}" не найдена!')
        answer = input('Создать директорию?[Yy/Nn] ')
        if answer in 'Yy':
            os.makedirs(path)
            return True
    # Если ничего нет и нам ничего не нужно
    return False


def fill_vacancies_list(filename: str, start: str = None, stop: str = None):
    """Заполняем очередь.

    :param filename: Название файла с вакансиями для заполнения очереди
    :param start: Строка-название вакансии с которой необходимо начинать
    :param stop: Строка-название вакансии на которой необходимо заканчивать
    :return: queue.Queue
    """
    print('Идет заполнение очереди вакансий.')
    q = Queue()
    with open(filename, 'r') as f:
        start_flag = False if start else True
        for url in f.readlines():
            url = url.strip()
            if stop and stop in url:
                break
            if start and start in url:
                start_flag = True
            if start_flag:
                if url in q.queue:
                    continue
                q.put(url)
    print(f'Очередь заполнена({q.qsize()} вакансий).')
    return q


def save_logo(working_directory: str, url: str) -> str:
    """Сохранение лого компании.

    :param working_directory: Путь к рабочей директории для сохранения
    :param url: url изображения для скачивания
    :return: Название файла
    """
    filename = url.split('/')[-1]
    file_path = os.path.join(working_directory, 'img', filename)
    if os.path.exists(file_path):
        return filename
    response = requests.get(url, stream=True)
    with open(file_path, 'wb') as f:
        for chunk in response.iter_content(2000):
            f.write(chunk)
    return filename


def salary_str_to_list(salary: str) -> list:
    """Волшебная функция возвращающая список из числовых значений зарплаты
    [] - зарплата не указана
    [some_number] - указана или зарплата одним значением или вместе с
    'от', 'до'
    [some-number, another-number] - указан диапазон
    :param salary: Строка содержащая з/п вакансии
    :return: list
    """
    return list(map(int, re.findall(r'\d+', salary)))


def random_sleep(a: int = 1, b: int = 2):
    """Задержка несколько секунд"""
    time.sleep(random.randint(a, b))
