import os
import time

import progressbar
import requests
from bs4 import BeautifulSoup

from parser_tools import utils
from .vars import MAX_MONEY_K, SUPERJOB_SPEC_LINK, HEADERS, CATALOGUES, \
    MAX_MONEY


def _get_pages_count(page: str) -> int:
    """Возвращает количество страниц для парсинга."""
    print('Расчет количества страниц..')
    soup = BeautifulSoup(page, 'html.parser')
    pages = soup.findAll('a', 'Paginator_link')[-1].text
    print(f'Найдено {pages} страниц')
    return int(pages)


def _is_valid_vacancy(vacancy):
    """Проверка вакансии на валидность по зарплате."""
    price = vacancy.find('div', {'class': 'LandingPageListElement_price'})
    price = price.get_text().replace(' Р', '').strip()

    link = vacancy.find('a', {'class': 'LandingPageListElement_link'})['href']
    if 'По договоренности' in price:
        return True, link
    price_list = price.split()
    if int(price_list[1]) <= MAX_MONEY_K or price_list[0] == 'до':
        return True, link

    return False, link


def accumulate_links(filename, super_url, page_count, key='&page='):
    """Сбор валидных вакансий в список ссылок."""
    print('Сбор валидных вакансий:')
    with open(filename, 'a') as f:
        url_with_page_key = super_url + key
        for page_num in reversed(range(1, page_count + 1)):
            print(f'Страница {page_num} из {page_count}')
            link = f'{url_with_page_key}{page_num}'
            # TODO: exception
            page = requests.get(link)
            soup = BeautifulSoup(page.text, 'html.parser')
            # Поиск всех вакансий на странице
            vacancies = soup.find_all(
                'div', {'class': 'LandingPageListElement'})
            # Поиск зарплаты в вакансиях
            for vacancy in vacancies:
                valid, vacancy_url = _is_valid_vacancy(vacancy)
                if valid:
                    vacancy_id = vacancy_url.split('-')[-1]\
                        .replace('.html', '')
                    f.write(vacancy_id + os.linesep)


def list_creator(filename: str):
    """Сбор и запись вакансий в файл.

    :param filename: название файла для записи валидных вакансий
    """
    for link in SUPERJOB_SPEC_LINK:
        print(f'\nСсылка категории вакансий: \n{link}\n')
        page = requests.get(link).text
        page_count = _get_pages_count(page)
        accumulate_links(filename, link, page_count)


def parse_vacancy(vacancy: dict, separator='|||') -> tuple:
    """Разбираем что тут у нас."""
    if 'error' in vacancy.keys():
        return False, False
    vacancy_title = vacancy['profession']

    employer = vacancy['client']  # razdel rabotodatelya
    employer_url = employer.get('link', None)
    employer_name = employer.get('title', None)

    employer_image_url = employer.get('client_logo', None)
    employer_image_filename = \
        employer_image_url.split('/')[-1] if employer_image_url else None

    employer_contact_phone = vacancy['phone']
    employer_contact_second_phone = None
    employer_contact_name = vacancy['contact']
    employer_contact_email = vacancy.get('email', None)

    vacancy_salary_from = vacancy['payment_from']
    vacancy_salary_to = vacancy['payment_to']
    vacancy_salary = f'{vacancy_salary_from}-{vacancy_salary_to}'

    vacancy_city = vacancy['town']['title']
    vacancy_address = vacancy['address']
    vacancy_url = vacancy['link']

    vacancy_description = vacancy['work'] if vacancy['work'] else ' '
    vacancy_description += vacancy['compensation'] if vacancy['compensation'] else ' '
    vacancy_description += vacancy['candidat'] if vacancy['candidat'] else ' '

    employer_address = None

    vacancy_inline = f'{employer_name} {separator} ' \
                     f'{employer_contact_name} {separator} ' \
                     f'{employer_contact_phone} {separator} ' \
                     f'{employer_contact_second_phone} {separator} ' \
                     f'{employer_contact_email} {separator} ' \
                     f'{vacancy_title} {separator} ' \
                     f'{vacancy_description} {separator} ' \
                     f'{vacancy_salary} {separator} ' \
                     f'{vacancy_city} {separator} ' \
                     f'{vacancy_address} {separator} ' \
                     f'{employer_address} {separator} ' \
                     f'{vacancy_url} {separator} ' \
                     f'{employer_url} {separator} ' \
                     f'{employer_image_filename}'.replace('\n', ' ')
    return vacancy_inline, employer_image_url


def _fetch_vacancy(url: str, headers: dict) -> dict:
    """Получение json вакансии.
    Слип нужен т.к. разрешено 120 запросов в минуту с 1 ip.
    """
    try:
        time.sleep(.3)
        response = requests.get(url, headers=headers)
    except ConnectionError:
        print('sleep')
        time.sleep(60)
        response = requests.get(url, headers=headers)
    return response.json()


def parse_function(vacancies_list: None, working_directory: str,
                   output_file: str) -> None:
    """Проход по всем вакансиям в списке вакансий, разбор с записью в файл и
    скачиванием лого компании.

    :param vacancies_list: В данном случае не нужен
    :param working_directory: Директория для сохранения результатов
    :param output_file: Файл для сохранения
    """
    queue_len = len(CATALOGUES)
    bar = progressbar.ProgressBar(max_value=queue_len, redirect_stdout=True)
    file_path = os.path.join(working_directory, output_file)
    # Проход по всем категориям
    for catalog in CATALOGUES:
        page_num = 0
        with open(file_path, 'a') as f:
            bar.update(bar.value + 1)
            while True:
                url = f'https://api.superjob.ru/2.0/vacancies/' \
                      f'?order_field=date&payment_from=0' \
                      f'&payment_to={MAX_MONEY}&period=1&count=100' \
                      f'&catalogues={catalog}&page={page_num}'
                print(url)
                response = _fetch_vacancy(url, headers=HEADERS)
                for vacancy in response['objects']:
                    values, logo_url = parse_vacancy(vacancy)
                    if values:
                        f.write(f'{values}{os.linesep}')
                    if logo_url:
                        utils.save_logo(working_directory, logo_url)
                # Если есть еще страница с результатом поиска
                if response['more']:
                    page_num += 1
                else:
                    break
    bar.finish()
