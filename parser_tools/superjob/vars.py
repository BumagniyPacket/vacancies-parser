MAX_MONEY = 50_000
MAX_MONEY_K = 50
VALID_VACANCIES_FILE = 'superjob_vacancies.txt'

SUPERJOB_SPEC_LINK = [
    'https://www.superjob.ru/vakansii/it-internet-svyaz-telekom/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/administrativnaya-rabota-sekretariat-aho/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/banki-investicii-lizing/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/bezopasnost-sluzhby-ohrany/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/buhgalteriya-finansy-audit/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/gosudarstvennaya-sluzhba/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/dizajn/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/domashnij-personal/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/zakupki-snabzhenie/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/iskusstvo-kultura-razvlecheniya/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/kadry-upravlenie-personalom/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/konsalting-strategicheskoe-razvitie/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/marketing-reklama-pr/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/medicina-farmacevtika-veterinariya/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/nauka-obrazovanie-povyshenie-kvalifikacii/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/nekommercheskie-organizacii-volonterstvo/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/prodazhi/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/yurisprudenciya/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/uslugi-remont-servisnoe-obsluzhivanie/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/turizm-gostinicy-obschestvennoe-pitanie/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/transport-logistika-ved/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/top-personal/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/syre/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/stroitelstvo-proektirovanie-nedvizhimost/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/strahovanie/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/sport-fitnes-salony-krasoty-spa/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/smi-izdatelstva/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/selskoe-hozyaistvo/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/promyshlennost-proizvodstvo/?geo%5Bc%5D%5B0%5D=1',
    'https://www.superjob.ru/vakansii/rabochii-personal/?geo%5Bc%5D%5B0%5D=1'
]

HEADERS = {
    'Host': 'api.superjob.ru',
    'X-Api-App-Id': 'v1.r07827d7bda2abdf45152076fee6a0a7a5f0643eae20298c39'
                    '817c16219825f1872f621db.e1b494df31f77b145e2e2614f84dc'
                    '33fa52c844c',
    'Authorization': 'Bearer r.000000125992315.69cc0dfb7c719cc18bba43a5723'
                     'fa8bd23a12e6f.f002445885daa130343b18e625bc70b53abfd3'
                     '7d',
    'Content-Type': 'application/x-www-form-urlencoded'
}
CATALOGUES = [33, 1, 381, 182, 11, 151, 62, 471, 512, 205, 76,
              426, 234, 136, 270, 175, 438, 327, 505, 548, 222,
              260, 284, 306, 414, 478, 86, 197, 362, 100]
