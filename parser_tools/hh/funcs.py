import os
import progressbar
import requests

from parser_tools import utils
from .vars import CURRENCY_DICT, SPECIALISATIONS_RANGE


def _is_valid_vacancy(vacancy_salary: dict, max_salary: int = 49_999) -> bool:
    """Проверка на валидность по зарплате."""
    if not vacancy_salary:
        return True
    currency = vacancy_salary['currency']

    salary_from = vacancy_salary['from']
    if salary_from:
        salary_from *= CURRENCY_DICT[currency]

    salary_to = vacancy_salary['to']
    if salary_to:
        salary_to *= CURRENCY_DICT[currency]

    if salary_from and salary_from < max_salary:
        return True
    elif salary_to and salary_to < max_salary:
        return True
    return False


def _page_processing(page: dict, filename: str):
    """Проверка вакансий на валидность и запись в файл."""
    vacancies = page['items']
    with open(filename, 'a') as f:
        for vacancy in vacancies:
            salary = vacancy['salary']
            vacancy_id = vacancy['id']
            if _is_valid_vacancy(salary):
                f.write(vacancy_id + os.linesep)


def parse_vacancy(vacancy: dict, separator: str='|||') -> tuple:
    """Разбираем что тут у нас."""
    # Если описание вакансии содержит ошибки она невалидна
    if 'errors' in vacancy.keys():
        return False, False
    if vacancy['archived']:  # or vacancy['errors']:
        return False, False
    else:
        vacancy_title = vacancy['name']
        # Раздел работодателя
        employer = vacancy['employer']
        employer_url = employer.get('alternate_url', None)
        employer_name = employer.get('name', None)

        # Раздел контактов работодателя
        contacts = vacancy['contacts']
        employer_contact_second_phone = None
        if contacts:
            phones = [f'{phone["country"]}({phone["city"]}){phone["number"]}'
                      for phone in contacts['phones']]
            employer_contact_phone = phones[0] if len(phones) else None
            if len(phones) > 1:
                employer_contact_second_phone = phones[1]

            employer_contact_name = contacts['name']
            employer_contact_email = contacts['email']
        else:
            employer_contact_phone = None
            employer_contact_name = None
            employer_contact_email = None

        # Раздел зарплаты
        vacancy_salary = None
        salary = vacancy['salary']
        if salary:
            vacancy_salary = f'{salary["from"]}-{salary["to"]}'

        # Раздел адрес
        employer_address = None
        address = vacancy['address']
        if address:
            vacancy_city = address['city']
            vacancy_address = (address['street'], address['building'])
        else:
            vacancy_city = vacancy['area']['name']
            vacancy_address = None

        vacancy_url = vacancy['alternate_url']
        vacancy_description = vacancy['description']

        # Лого работодателя
        employer_image_url = employer.get('logo_urls', None)
        employer_image_filename = None
        if employer_image_url:
            employer_image_url = employer_image_url['original']
            employer_image_filename = employer_image_url.split('/')[-1]

        vacancy_inline = f'{employer_name}{separator}' \
                         f'{employer_contact_name}{separator}' \
                         f'{employer_contact_phone}{separator}' \
                         f'{employer_contact_second_phone}{separator}' \
                         f'{employer_contact_email}{separator}' \
                         f'{vacancy_title}{separator}' \
                         f'{vacancy_description}{separator}' \
                         f'{vacancy_salary}{separator}' \
                         f'{vacancy_city}{separator}' \
                         f'{vacancy_address}{separator}' \
                         f'{employer_address}{separator}' \
                         f'{vacancy_url}{separator}' \
                         f'{employer_url}{separator}' \
                         f'{employer_image_filename}'.replace('\n', ' ')
        return vacancy_inline, employer_image_url


def _fetch_vacancy(vacancy_id: str) -> dict:
    """Получение словарая вакансии по id."""
    url = f'https://api.hh.ru/vacancies/{vacancy_id}'
    return requests.get(url).json()


def list_creator(filename: str, area: int=113, today: bool=True):
    """Поочередный проход по списку из специальностей и составление файлового
    списка валидных вакансий для парсинга.

    :param filename: Название файла для записи результатов
    :param area: Зона для поиска вакансий(по умолчанию Россия)
    :param today: Нужно ли искать только свежие вакансии
    """
    if today:
        # Момент с периодом как работает?
        request_url = f'https://api.hh.ru/vacancies?per_page=100' \
                      f'&order_by=publication_time&period=1' \
                      f'&area={area}&specialization='
    else:
        request_url = f'https://api.hh.ru/vacancies?per_page=100' \
                      f'&area={area}&specialization='
    # Шагаем по специализациям
    for i in range(1, SPECIALISATIONS_RANGE + 1):
        # Первая страница для поиска количества страниц
        url = f'{request_url}{i}'
        print(f'\nСпециальность {i} из {SPECIALISATIONS_RANGE}: {url}\n')
        response = requests.get(url).json()
        pages = response['pages']
        # Проходим все страницы в обратном порядке
        # Это для того, чтобы избежать дублирования вакансий в случае
        # добавления новой
        for page in reversed(range(pages)):
            print(f'Обработка страниц. Осталось {page} страниц')
            response_by_page = requests.get(f'{url}&page={page}').json()
            _page_processing(response_by_page, filename)


def parse_function(vacancies_list, working_directory: str, output_file: str):
    """Проход по всем вакансиям в списке вакансий, разбор с записью в файл и
    скачиванием лого компании.

    :param vacancies_list: очередь вакансий для разбора
    :param working_directory: директория для сохренения результов
    :param output_file: название файла для сохранения результов
    """
    queue_len = vacancies_list.qsize()
    bar = progressbar.ProgressBar(max_value=queue_len, redirect_stdout=True)
    file_path = os.path.join(working_directory, output_file)

    with open(file_path, 'a') as f:
        while not vacancies_list.empty():
            vacancy_id = vacancies_list.get()
            print(vacancy_id)
            vacancy = _fetch_vacancy(vacancy_id)
            values, logo_url = parse_vacancy(vacancy)
            if values:
                f.write(f'{values}{os.linesep}')
            if logo_url:
                utils.save_logo(working_directory, logo_url)
            num = queue_len - vacancies_list.qsize()
            bar.update(num)
    bar.finish()
