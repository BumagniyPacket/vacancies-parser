SPECIALISATIONS_RANGE = 27  # start from 1
AREAS = [5, 113, 40, 16]  # countries

CURRENCY_DICT = {
    'RUR': 1,
    'UAH': 2.1,
    'USD': 58,
    'EUR': 68,
    'KGS': 0.85,
    'KZT': 0.17,
    'BYN': 29.5,
    'BYR': 29.5
}
