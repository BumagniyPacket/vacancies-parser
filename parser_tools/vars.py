"""Файл для хранения переменных системы."""

HHRU = 1
SUPERJOB = 2
RABOTA = 3
RDWRU = 4
TRUDVSEM = 6
AVITO = 5
ZARPLATA = 7

SET_OF_RESOURCES = {
    HHRU,
    AVITO,
    RABOTA,
    RDWRU,
    SUPERJOB,
    TRUDVSEM,
    ZARPLATA
}
