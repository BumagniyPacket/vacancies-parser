import os
import re

import progressbar
import requests

from bs4 import BeautifulSoup

from .vars import SALARY_MAX, STATES
from parser_tools import utils


def _process_salary(soup) -> str:
    """Приведение зарплаты в общий вид."""
    vacancy_salary_text = soup.replace(' ', '')
    vacancy_salary_nums = utils.salary_str_to_list(vacancy_salary_text)
    if not vacancy_salary_nums:
        return ''
    if len(vacancy_salary_nums) == 1:
        if 'от' in vacancy_salary_text:
            return f'{vacancy_salary_nums[0]}-..'
        elif 'до' in vacancy_salary_text:
            return f'0-{vacancy_salary_nums[0]}'
        return f'{vacancy_salary_nums[0]}'
    return f'{vacancy_salary_nums[0]}-{vacancy_salary_nums[1]}'


def _is_valid_vacancy(vacancy_salary: str) -> bool:
    """Проверка на валидность вакансии по заработной плате.
    Если з/п не указана вернуть True
    Если указан диапазон, он не должен превышать максимальной указанной з/п,
    иначе вернуть False.
    Если указана одно число оно не должно превышать максимальную з/п.

    :param vacancy_salary: Строка с указанным в ней уровнем зарплаты
    :return: Вакансия валидна да/нет
    """
    vacancy_salary = vacancy_salary.replace(' ', '')
    if any(_ in vacancy_salary for _ in '$€т'):
        return False
    vacancy_salary_nums = utils.salary_str_to_list(vacancy_salary)
    if not vacancy_salary_nums:
        return True
    if len(vacancy_salary_nums) == 1:
        if vacancy_salary_nums[0] < SALARY_MAX:
            return True
    if vacancy_salary_nums[0] < SALARY_MAX:
        return True
    return False


def _page_processing(page, filename: str, today: bool):
    """Разбор страницы с вакансиями.
    Для начала идет поиск по всем блокам с вакансиями. Далее проходя по этому
    списку идет проверка даты в случае необходимости. Если today=True и
    вакансия выложена ранее, то идет возврат значения True из функции, тем
    самым показывая, что данная страница была последней.
    Если today=False происходит валидация вакансии по заработной плате, в
    случее возврата True функцией _is_valid_vacancy, производится запись
    ссылки на функцию в файл.

    :param page: BS страницы для обработки
    :param filename: Название файла для записи результатов
    :param today: Валидация вакансии по дате выгрузки
    :return: bool: True если страница последняя
    """
    vacancies = page.find_all(
        'section', {'class': 'list-vacancies__item'})

    with open(filename, 'a') as f:
        for vacancy in vacancies:
            today_flag = vacancy.find('div', 'list-vacancies__date') \
                .text.strip()
            if today and today_flag.lower() != 'сегодня':
                return True

            vacancy_salary = vacancy.find(
                'div', {'class': 'list-vacancies__salary'}).text.strip()
            if _is_valid_vacancy(vacancy_salary):
                valid_vacancy_url = vacancy.find(
                    'a', {'class': 'list-vacancies__title'})['href']
                vacancy_url = f'https://www.rabota.ru{valid_vacancy_url}'
                f.write(vacancy_url + os.linesep)


def _state_processing(state: str, filename: str, today: bool):
    """Проход по всем страницам штата(округа).
    На ресурсе реализован механизм инфинити скролла, для этого идет проверка
    на наличие кнопки подгрузки следующих результатов. При наличии этой
    кнопки осуществляется загрузка новых элементов и дальнейший разбор. В
    противном случае заканчивается выполнение функции.

    :param state: штат(округ) по которому будет произведен поиск
    :param filename: Файл для записи результатов
    :param today: Поиск только по вакансиям добавленным сегодня.
    """
    if today:
        url = f'https://{state}.rabota.ru/vacancy/прямой работодатель/' \
              f'?page=1&period=1&sort=date'
    else:
        url = f'https://{state}.rabota.ru/vacancy/прямой работодатель/?page=1'

    is_last_loop = False
    while not is_last_loop:
        print(url)
        page = requests.get(url).text
        page_soup = BeautifulSoup(page, 'html.parser')
        is_last_loop = _page_processing(page_soup, filename, today=today)

        # инфинити скролл
        next_button = page_soup.find(
            'a', {'class': 'list-vacancies__more', 'type': 'button'})
        if next_button:
            url = f'https:{next_button["href"]}'
        else:
            break


def list_creator(filename: str, today: bool=True):
    """Сбор вакансий с сайта, поочередно, по каждому state."""
    for state in STATES:
        _state_processing(state, filename, today=today)


def _fetch_vacancy(vacancy_url: str) -> str:
    """Получение текстового представления вакансии."""
    return requests.get(vacancy_url).text


def parse_vacancy(vacancy: str, url: str, separator: str='|||') -> tuple:
    """Приведение вакансии в необходимое строчное представление.

    :param vacancy: html-представление вакансии
    :param url: url на текущую вакансию
    :param separator: разделитель для полей
    :return: tuple(str, str): Кортеж, где первый элемент - строка результат
             выполнения функции парсинга, ссылка на лого работодателя
             (если есть)
    """
    soup = BeautifulSoup(vacancy, 'html.parser')
    if soup.find('span', text=re.compile('Вакансия удалена')):
        return False, False

    vacancy_title = soup.find('span', {'data-fox': 'targeting'}).text.strip()
    vacancy_description = '<br />'.join(
        soup.find('div', {'class': 'card-vacancy__desc'}).text.splitlines())
    vacancy_salary = _process_salary(
        soup.find('div', {'class': 'card-vacancy__salary'}).text)

    employer_contact_name = None
    employer_contact_phone = None
    employer_contact_second_phone = None
    # элемент который есть только у вакансий с контактами
    employer_contacts_elem = soup.find(
        'div', {'id': 'modal-vacancy-contacts'})
    if employer_contacts_elem:
        # блок с контактной информацией
        employer_contacts = employer_contacts_elem \
            .find('div', {'class': 'modal-body'})
        # блок который есть при наличии контактного лица
        employer_contact_name_container = employer_contacts \
            .find('div', text=re.compile('Контактное лицо'))
        if employer_contact_name_container:
            employer_contact_name = employer_contact_name_container \
                .find_next('div').text.strip()
        # телефоны
        phones = employer_contacts.find_all('a', {'class': 'text-black'})
        employer_contact_phone = phones[0].text if phones else None
        employer_contact_second_phone = phones[1].text if len(phones) > 1 \
            else None

    # основная информация о работодателе
    employer_info = soup.find('header', {'class': 'card-company__header'})
    employer_name = None
    employer_url = None
    employer_title_element = employer_info \
        .find('div', {'class': 'card-company__title'})
    # блок с названием компании и ссылка на нее в системе
    if employer_title_element:
        if employer_title_element.a:
            employer_name = employer_title_element.a.text.strip()
            employer_url = \
                f'https://rabota.ru{employer_title_element.a["href"]}'
        else:
            employer_name = employer_title_element.text.strip()

    vacancy_address_elem = soup.find('div', {'class': 'metro-station__title'})
    if vacancy_address_elem:
        vacancy_address_text = vacancy_address_elem.text.strip()
        vacancy_address = ' '.join(vacancy_address_text.splitlines())
    else:
        vacancy_address = None

    employer_logo_elem = soup.find('div', {'class': 'branding__logo'})
    if hasattr(employer_logo_elem, 'a'):
        # [2:] нужно тк любой линк начинается с '//'
        employer_image_url = f'https://{employer_logo_elem.a.img["src"][2:]}'
        employer_image_filename = employer_image_url.split('/')[-1]
    else:
        employer_image_url = None
        employer_image_filename = None

    # элементы которых нет в вакансиях
    vacancy_url = url
    vacancy_city = None
    employer_address = None
    employer_contact_email = None

    vacancy_inline = f'{employer_name} {separator} ' \
                     f'{employer_contact_name} {separator} ' \
                     f'{employer_contact_phone} {separator} ' \
                     f'{employer_contact_second_phone} {separator} ' \
                     f'{employer_contact_email} {separator} ' \
                     f'{vacancy_title} {separator} ' \
                     f'{vacancy_description} {separator} ' \
                     f'{vacancy_salary} {separator} ' \
                     f'{vacancy_city} {separator} ' \
                     f'{vacancy_address} {separator} ' \
                     f'{employer_address} {separator} ' \
                     f'{vacancy_url} {separator} ' \
                     f'{employer_url} {separator} ' \
                     f'{employer_image_filename}'.replace('\n', ' ')
    return vacancy_inline, employer_image_url


def parse_function(vacancies_list, working_directory: str, output_file: str):
    """Проход по всем вакансиям в списке вакансий, разбор с записью в файл и
    скачиванием лого компании.

    :param vacancies_list: очередь вакансий для разбора
    :param working_directory: директория для сохренения результов
    :param output_file: название файла для сохранения результов
    """
    queue_len = vacancies_list.qsize()
    bar = progressbar.ProgressBar(max_value=queue_len, redirect_stdout=True)
    file_path = os.path.join(working_directory, output_file)

    with open(file_path, 'a') as f:
        while not vacancies_list.empty():
            vacancy_url = vacancies_list.get()
            print(vacancy_url)
            vacancy = _fetch_vacancy(vacancy_url)
            values, logo_url = parse_vacancy(vacancy, vacancy_url)
            if values:
                f.write(f'{values}{os.linesep}')
            if logo_url:
                utils.save_logo(working_directory, logo_url)
            num = queue_len - vacancies_list.qsize()
            bar.update(num)
    bar.finish()
