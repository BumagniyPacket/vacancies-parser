"""Парсинг вакансий zarplata.ru."""
import os
import requests

from parser_tools import utils
from .vars import MAX_PAGES, MAX_SALARY


def _is_valid_vacancy(vacancy_salary: tuple) -> bool:
    """Проверка вакансии на соответствие з/п."""
    salary_min, salary_max = vacancy_salary
    if not salary_max and not salary_min:
        return True
    if salary_min and salary_min < MAX_SALARY:
        return True
    if not salary_min and salary_max < MAX_SALARY:
        return True
    return False


def parse_vacancy(vacancy: dict, separator: str = '|||') -> tuple:
    """Преобразование вакансии в необходимый вид."""
    vacancy_title = vacancy['header']
    vacancy_url = f'https://zarplata.ru{vacancy["url"]}'
    vacancy_description = vacancy['description']

    employer_contacts = vacancy['contact']
    employer_address = employer_contacts['address']

    employer_contact_phone = None
    employer_contact_second_phone = None
    if employer_contacts['phones']:
        employer_contact_phone = employer_contacts['phones'][0]['phone']
        if len(employer_contacts['phones']) != 1:
            employer_contact_second_phone = \
                employer_contacts['phones'][1]['phone']

    employer_contact_name = employer_contacts['name']
    employer_contact_email = employer_contacts['email']

    employer = vacancy['company']
    employer_name = employer['title']
    employer_url = f'https://www.zarplata.ru/companies/{employer["id"]}'

    employer_image_url = employer['logo']['url'] if employer['logo'] else None
    employer_image_filename = employer_image_url.split('/')[-1] \
        if employer_image_url else None

    vacancy_salary = f'{vacancy["salary_min_rub"]}-{vacancy["salary_max_rub"]}'
    vacancy_city = employer_contacts['city']['title'] \
        if employer_contacts['city'] else None

    address = vacancy['address']
    if address:
        vacancy_address = f'{address["city"]["title"]}, {address["street"]},' \
                          f' {address["building"]}'
    else:
        vacancy_address = None

    vacancy_inline = f'{employer_name} {separator} ' \
                     f'{employer_contact_name} {separator} ' \
                     f'{employer_contact_phone} {separator} ' \
                     f'{employer_contact_second_phone} {separator} ' \
                     f'{employer_contact_email} {separator} ' \
                     f'{vacancy_title} {separator} ' \
                     f'{vacancy_description} {separator} ' \
                     f'{vacancy_salary} {separator} ' \
                     f'{vacancy_city} {separator} ' \
                     f'{vacancy_address} {separator} ' \
                     f'{employer_address} {separator} ' \
                     f'{vacancy_url} {separator} ' \
                     f'{employer_url} {separator} ' \
                     f'{employer_image_filename}'.replace('\n', ' ')

    return vacancy_inline, employer_image_url


def _fetch_vacancy(vacancy_url: str) -> dict:
    """Получение текстового представления вакансии."""
    response = requests.get(vacancy_url)
    return response.json()


def _page_processing(page: dict, filename: str, parse: bool,
                     working_directory: str):
    """Обработка строки с вакансиями."""
    with open(filename, 'a') as f:
        for vacancy in page['vacancies']:
            if _is_valid_vacancy((vacancy['salary_min_rub'],
                                  vacancy['salary_max_rub'])):
                vacancy_url = f'http://api.zp.ru/v1/vacancies/{vacancy["id"]}'
                if parse:
                    values, logo_url = parse_vacancy(vacancy)
                    if values:
                        f.write(f'{values}{os.linesep}')
                    if logo_url:
                        utils.save_logo(working_directory, logo_url)
                else:
                    f.write(f'{vacancy_url}{os.linesep}')


def list_creator(filename: str, today: bool=True, parse: bool=False,
                 working_directory: str=None):
    """Создание списка валидных вакансий с записью в файл."""
    if today:
        base_url = f'https://api.zp.ru/v1/vacancies?limit=100&period=today' \
                   f'&is_new_only=1&'
    else:
        base_url = f'https://api.zp.ru/v1/vacancies?limit=100'
    for i in reversed(range(MAX_PAGES)):
        offset = i * 100
        url = f'{base_url}offset={offset}'
        print(url)

        page = requests.get(url).json()
        _page_processing(page, filename, parse, working_directory)


def parse_function(vacancies_list, working_directory: str, output_file: str):
    """Проход по всем вакансиям в списке вакансий, разбор с записью в файл и
    скачиванием лого компании."""
    file_path = os.path.join(working_directory, output_file)
    list_creator(file_path, parse=True, working_directory=working_directory)
