import time
import os

import requests

from bs4 import BeautifulSoup
from requests import ReadTimeout
from requests.exceptions import ConnectionError

from .vars import MAX_PAGES, HEADERS, CATEGORIES, REGION, COOKIES
from parser_tools import utils


def _pages_counter(page: str) -> int:
    """Количество страниц с вакансиями."""
    soup = BeautifulSoup(page, 'html.parser')
    page_counter = soup.find('div', {'class': 'nav-helper-text'}) \
        .text.replace(' ', '')
    count = int(page_counter) // 100 + 1
    if count > MAX_PAGES:
        return MAX_PAGES
    return count


def _page_processing(page: str, today: bool) -> tuple:
    """Запись ссылок на валидные вакансии в файл."""
    soup = BeautifulSoup(page, 'html.parser')
    vacancies = []
    last = False
    for vacancy in soup.find_all('div', {'class': 'item'}):
        if today:
            date = vacancy.find('span', 'date').text
            if 'сегодня' not in date.lower():
                last = True
                continue

        vacancy_url_elem = vacancy.find('a', 'description-title-link')
        vacancy_url = 'https://m.avito.ru' + vacancy_url_elem['href']
        vacancies.append(vacancy_url)

    return vacancies, last


def _page_crawler(category: str, pages_count: int, filename: str, today: bool):
    """Проход по станицам категории."""
    with open(filename, 'a') as f:
        for i in range(1, pages_count + 1):
            url = f'https://www.avito.ru/{REGION}/vakansii/{category}' \
                  f'?pmax=50000&pmin=0&s=104&view=list&p={i}'
            utils.random_sleep()
            page = requests.get(url, headers=HEADERS).text
            print(url)
            vacancies, last = _page_processing(page, today)
            for vacancy in vacancies:
                f.write(vacancy + os.linesep)
            if last:
                break


def list_creator(filename: str, today: bool=True):
    """Сбор вакансий с сайта, поочередно, по каждой категории."""
    for category in CATEGORIES:
        category_url = f'https://m.avito.ru/{REGION}/vakansii/{category}' \
                       f'?pmax=50000&pmin=0&view=list'
        print('Категория:', category)
        print(category_url)
        utils.random_sleep()
        category_page = requests.get(category_url, headers=HEADERS).text
        if 'firewall-container' in category_page:
            raise Exception('A teper dostup zablokirovali)0))))00))0))0')

        pages_count = _pages_counter(category_page)
        _page_crawler(category, pages_count, filename, today)


def _get_phone(url: str, referer: str, proxy: dict, cookies: str) -> dict:
    headers = {
        'Host': 'm.avito.ru',
        'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:56.0) '
                      'Gecko/20100101 Firefox/56.0',
        'Accept': 'application/json',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate, br',
        'X-Requested-With': 'XMLHttpRequest',
        'Referer': referer,
        'Cookie': cookies,
        'Connection': 'keep-alive'
    }

    try:
        r = requests.get(url, timeout=10, headers=headers, proxies=proxy)
    except ConnectionError:
        time.sleep(2)
        r = requests.get(url, headers=headers, proxies=proxy)
    except ReadTimeout:
        r = requests.get(url, headers=headers, proxies=proxy)
    return r.json()


def _read_phone(soup, url: str, proxy: dict, cookies: str) -> str:
    """Получение телефона работодателя."""
    temp = soup.find("a", "action-show-number")
    phone_link = f'https://m.avito.ru{temp["href"]}?async'
    time.sleep(1)
    try:
        phone = _get_phone(phone_link, url, proxy, cookies)['phone']
    except KeyError:
        return _read_phone(soup, url, proxy, cookies)
    return phone


def parse_vacancy(vacancy: str, url: str, proxy: dict=None,
                  cookies: str=COOKIES, separator: str='|||') -> tuple:
    """Приведение вакансии в необходимое строчное представление."""
    soup = BeautifulSoup(vacancy, 'html.parser')

    vacancy_name = soup.find('header', {'class': 'single-item-header'})
    if not vacancy_name:
        return None, None
    vacancy_title = vacancy_name.text.strip()

    employer_name = soup.find('a', {'class': 'person-name-link'})
    if employer_name:
        employer_url = 'https://avito.ru%s' % employer_name['href']
        employer_name = employer_name.text.strip()
    else:
        employer_name = soup.find('div', {'class': 'person-name'}) \
            .text.strip()
        employer_url = None

    employer_contact_name = soup.find('div', {'class': 'person-name-text'})
    if employer_contact_name:
        employer_contact_name = employer_contact_name.text.strip()

    employer_contact_phone = _read_phone(soup, url, proxy, cookies)

    employer_contact_second_phone = None
    employer_contact_email = None
    vacancy_description = soup.find(
        'div', {'class': 'description-preview-wrapper'}).text.strip()

    vacancy_salary = soup.find('span', {'class': 'price-value'}).text.strip() \
        .replace('\xa0', '')
    if 'Зарплата' in vacancy_salary:
        vacancy_salary = None

    vacancy_city = None

    vacancy_address = ' '.join([_.text.strip() for _ in soup.find(
        'div', {'class': 'person-address'}).find_all('span')])

    employer_address = None
    vacancy_url = url

    employer_image_url = soup.find('link', {'rel': 'image_src'})['href']
    if 'avito.png' in employer_image_url:
        employer_image_url = None
        employer_image_filename = None
    else:
        employer_image_filename = employer_image_url.split('/')[-1]

    vacancy_inline = f'{employer_name} {separator} ' \
                     f'{employer_contact_name} {separator} ' \
                     f'{employer_contact_phone} {separator} ' \
                     f'{employer_contact_second_phone} {separator} ' \
                     f'{employer_contact_email} {separator} ' \
                     f'{vacancy_title} {separator} ' \
                     f'{vacancy_description} {separator} ' \
                     f'{vacancy_salary} {separator} ' \
                     f'{vacancy_city} {separator} ' \
                     f'{vacancy_address} {separator} ' \
                     f'{employer_address} {separator} ' \
                     f'{vacancy_url} {separator} ' \
                     f'{employer_url} {separator} ' \
                     f'{employer_image_filename}'.replace('\n', ' ')
    return vacancy_inline, employer_image_url


def _fetch_vacancy(vacancy_url: str, proxy: dict=None,
                   cookies: str=COOKIES) -> str:
    """Получение текстового представления вакансии."""
    headers = {
        'Host': 'm.avito.ru',
        'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:56.0) '
                      'Gecko/20100101 Firefox/56.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,'
                  '*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate, br',
        'Cookie': cookies,
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'
    }
    utils.random_sleep()
    try:
        r = requests.get(vacancy_url, timeout=10, headers=headers,
                         proxies=proxy).text
    except ConnectionError:
        time.sleep(2)
        r = requests.get(vacancy_url, headers=headers, proxies=proxy).text
    except ReadTimeout:
        r = requests.get(vacancy_url, headers=headers, proxies=proxy).text
    return r


def parse_function(vacancies_list, working_directory: str, output_file: str):
    """Проход по всем вакансиям в списке вакансий, разбор с записью в файл и
    скачиванием лого компании.

    :param vacancies_list: очередь вакансий для разбора
    :param working_directory: директория для сохренения результов
    :param output_file: название файла для сохранения результов
    """
    file_path = os.path.join(working_directory, output_file)

    with open(file_path, 'a') as f:
        while not vacancies_list.empty():
            vacancy_url = vacancies_list.get()
            print(vacancy_url)
            vacancy = _fetch_vacancy(vacancy_url)
            values, logo_url = parse_vacancy(vacancy, vacancy_url)
            if values:
                f.write(f'{values}{os.linesep}')
            if logo_url:
                utils.save_logo(working_directory, logo_url)
