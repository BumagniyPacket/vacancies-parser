"""Парсинг вакансий rdwru.ru."""
import datetime
import os
import progressbar
import re

import dateparser
import requests
from bs4 import BeautifulSoup

from parser_tools import utils
from .vars import MAIN_URL


def process_salary(salary: str) -> [str, bool]:
    """Приведение зарплаты в общий вид."""
    vacancy_salary_text = salary.replace(' ', '')
    vacancy_salary_nums = utils.salary_str_to_list(vacancy_salary_text)
    if not vacancy_salary_nums:
        return None
    if len(vacancy_salary_nums) == 1:
        if 'от' in vacancy_salary_text:
            return f'{vacancy_salary_nums[0]}-..'
        elif 'до' in vacancy_salary_text:
            return f'0-{vacancy_salary_nums[0]}'
        return f'{vacancy_salary_nums[0]}'
    return f'{vacancy_salary_nums[0]}-{vacancy_salary_nums[1]}'


def _page_counter(today: bool) -> int:
    """Расчет количества страниц.

    :return: Количество страниц
    """
    page = requests.get(f'{MAIN_URL}1').text
    soup = BeautifulSoup(page, 'html.parser')
    vacancy_count = int(soup.find('strong', {'id': 'vacancy_count'}).text)
    page_count = int(vacancy_count / 100) + 1
    if today:
        return page_count if page_count < 55 else 55
    return page_count


def _is_valid_date(date_elem: str, date: datetime) -> bool:
    """Функция сверки даты публикации вакансии с текущей. """
    published_date = dateparser.parse(date_elem).date()
    return published_date == date


def _page_processing(url: str, date: datetime.date, today: bool):
    """Сбор со страницы с вакансиями ссылки на них.

    :param url: str: ссылка на страницу
    :param date: дата на сегодня
    :param today: нужны ли вакансии только за сегодня
    :return: список вакансий
    """
    page_html = requests.get(url).text
    vacancies = BeautifulSoup(page_html, 'html.parser') \
        .find('div', {'class': 'search-results'}).find_all('li')
    for vacancy in vacancies:
        date_elem = vacancy.find('p', 'date')
        if not date_elem:
            continue
        vacancy_url = vacancy.find('div', {'class': 'like-h2'}).a['href']
        if today:
            if _is_valid_date(date_elem.text, date):
                continue
        yield f'https://rdw.ru{vacancy_url}'


def parse_vacancy(vacancy: str, url: str, separator: str = '|||') -> tuple:
    """Приведение вакансии в необходимое строчное представление.

    :param vacancy: html-представление вакансии
    :param url: url на текущую вакансию
    :param separator: разделитель для полей
    :return: tuple(str, str): Кортеж, где первый элемент - строка результат
             выполнения функции парсинга, ссылка на лого работодателя
             (если есть)
    """
    soup = BeautifulSoup(vacancy, 'html.parser')
    vacancy = soup.find('div', {'class': 'vacancy_card'})

    vacancy_title = soup.find('h1', {'itemprop': 'name'}).text.strip()
    # Поиск телефонов
    phones = soup.find('td', text=re.compile('Телефон'))
    if phones:
        phones_list = phones.parent.find_all('p', 'pb_10')
        phones_list = [_.contents[0].strip() for _ in phones_list]
        employer_contact_phone = phones_list[0]
        employer_contact_second_phone = phones_list[1] if \
            len(phones_list) > 1 else None
    else:
        employer_contact_phone = None
        employer_contact_second_phone = None
    # з/п
    vacancy_salary_elem = soup.find('dt', text=re.compile('зарплата'))
    # особый вид пробела
    vacancy_salary = process_salary(
        vacancy_salary_elem.parent.dd.text.strip().replace(' ', ''))
    # url
    vacancy_url = url.split('?')[0]
    # описание вакансии
    description_table = soup.find('table', {'class': 'vac_description_tbl'})
    if description_table.find(
            'th', text=re.compile('Основные данные о вакансии')):
        vacancy_description = '<br />'.join(
            _.text.strip() for _ in description_table.find_all('td'))
    else:
        vacancy_description = None

    employer_contact_name = soup.find('div', {'itemprop': 'contactPoints'}) \
                                .text.strip() or None
    employer_contact_email_elem = soup.find('a', {'itemprop': 'email'}).text
    employer_contact_email = employer_contact_email_elem or None

    employer_address = None
    employer_name = None
    employer_url = None
    employer_image_url = None
    employer_image_filename = None

    employer_name_elem = soup.find('td', text=re.compile('Компания'))
    if employer_name_elem:
        employer_name = employer_name_elem.parent.span.text.strip()
        employer_url = f'https://rdw.ru{employer_name_elem.parent.a["href"]}'
    employer_logo_elem = soup.find('img', 'employer-logo')
    if employer_logo_elem:
        employer_image_url = f'https://rdw.ru{employer_logo_elem["src"]}'
        employer_image_filename = employer_image_url.split('/')[-1]

    header = soup.find('div', {'class': 'vacancy_card'}) \
        .find('div', {'class': 'over_hid'})

    # TODO: ////
    if vacancy.find('div', {'class': 'dashed'}):
        location = soup.find_all('div', {'class': 'location_vacancy'})
        vacancy_region = header.find('div', {'class': 'center-col'}).span.text
        vacancy_address = f'{vacancy_region}, '
        for i in location:
            line = i.span.text.strip() if hasattr(i.span, 'text') else None
            vacancy_address += f'{line} ' if line else ' '

        vacancy_city_elem = location[0].span
        vacancy_city = vacancy_city_elem.text.strip() if hasattr(
            vacancy_city_elem, 'text') else None
    else:
        vacancy_city = header.find('div', {'class': 'center-col'}).span.text
        vacancy_address = None

    vacancy_inline = f'{employer_name} {separator} ' \
                     f'{employer_contact_name} {separator} ' \
                     f'{employer_contact_phone} {separator} ' \
                     f'{employer_contact_second_phone} {separator} ' \
                     f'{employer_contact_email} {separator} ' \
                     f'{vacancy_title} {separator} ' \
                     f'{vacancy_description} {separator} ' \
                     f'{vacancy_salary} {separator} ' \
                     f'{vacancy_city} {separator} ' \
                     f'{vacancy_address} {separator} ' \
                     f'{employer_address} {separator} ' \
                     f'{vacancy_url} {separator} ' \
                     f'{employer_url} {separator} ' \
                     f'{employer_image_filename}'.replace('\n', ' ')
    return vacancy_inline, employer_image_url


def _fetch_vacancy(vacancy_url: str, contacts: bool = True) -> str:
    """Получение текстового представления вакансии."""
    if contacts:
        response = requests.get(vacancy_url).text
        soup = BeautifulSoup(response, 'html.parser')
        is_archived = soup.find('td', text=re.compile('Вакансия в архиве'))
        if is_archived:
            return ''
        link = soup.find('a', {'class': 'bold text_14'})['href']
        return _fetch_vacancy(f'https://rdw.ru{link}', contacts=False)
    return requests.get(vacancy_url).text


def list_creator(filename: str, today: bool = True):
    """Создание списка вакансий.

    :param filename: название файла для записи
    :param today: вакансии только на сегодня
    """
    page_counts = _page_counter(today)
    date_today = datetime.date.today()
    with open(filename, 'a') as f:
        for page_num in reversed(range(page_counts)):
            page_url = f'{MAIN_URL}{page_num}'
            print(page_url)
            for vacancy_url in _page_processing(page_url, date_today, today):
                f.write(vacancy_url + os.linesep)


def parse_function(vacancies_list, working_directory: str, output_file: str):
    """Проход по всем вакансиям в списке вакансий, разбор с записью в файл и
    скачиванием лого компании.

    :param vacancies_list: очередь вакансий для разбора
    :param working_directory: директория для сохренения результов
    :param output_file: название файла для сохранения результов
    """
    queue_len = vacancies_list.qsize()
    bar = progressbar.ProgressBar(max_value=queue_len, redirect_stdout=True)
    file_path = os.path.join(working_directory, output_file)

    with open(file_path, 'a') as f:
        while not vacancies_list.empty():
            vacancy_url = vacancies_list.get()
            print(vacancy_url)
            vacancy = _fetch_vacancy(vacancy_url)
            if not vacancy:
                continue
            values, logo_url = parse_vacancy(vacancy, vacancy_url)
            if values:
                f.write(f'{values}{os.linesep}')
            if logo_url:
                utils.save_logo(working_directory, logo_url)
            num = queue_len - vacancies_list.qsize()
            bar.update(num)
    bar.finish()
