#!/usr/bin/env python
"""Настоящий инструмент для парсинга."""
import argparse
import os

from parser_tools.utils import validate_directory, get_parse_function_by_resource, \
    get_list_creation_function_by_resource, fill_vacancies_list, get_resource_name_by_id
from parser_tools.vars import SET_OF_RESOURCES, HHRU


def create_list(args):
    """Функция будет вызвана для создания списка вакансий"""
    resource = args.r
    filename = os.path.join(args.o)
    print(f'Выбран {filename} файл для записи')
    if os.path.exists(filename):
        print('Такой файл уже существует!')
        answer = input('Удалить?(В противном случае будет '
                       'произведена дозапись)[Yy/Nn] ')
        if answer in 'Yy':
            os.remove(filename)
    create_list_function = get_list_creation_function_by_resource(resource)
    create_list_function(filename)


def parse_list(args):
    """Функция будет вызвана для парсинга вакансий по списку"""
    if validate_directory(args.d, args.r):
        # Как оказалось не всем это нужно
        if args.i:
            vacancies_list = fill_vacancies_list(
                args.i, start=args.start, stop=args.stop)
        else:
            vacancies_list = None
        # TODO: sdelat' krasivee
        working_directory = os.path.join(args.d, get_resource_name_by_id(args.r))
        parse_function = get_parse_function_by_resource(args.r)
        parse_function(vacancies_list, working_directory, args.o)


def parse_args():
    """Настройка argparse"""
    parser = argparse.ArgumentParser(description='Тулза для парсинга работы')
    subparsers = parser.add_subparsers()
    # парсер для создания списка вакансий
    parser_list = subparsers.add_parser('list', help='Составление списка вакансий')
    # TODO: otet moment ti sam ponimaesh da?
    parser_list.add_argument('-r', choices=SET_OF_RESOURCES, default=HHRU, type=int,
                             help='Выбор ресурса для обработки')
    parser_list.add_argument('-o', default='list.txt',
                             help='Файл-список валидных вакансий')
    parser_list.set_defaults(func=create_list)
    # парсер для начала разбора вакансий
    parser_parse = subparsers.add_parser('parse', help='Парсинг вакансий по списку')
    parser_parse.add_argument('-i', help='Файл-список валидных вакансий')
    parser_parse.add_argument('-o', default='vacancies.txt', help='Файл для записи вакансий')
    parser.add_argument('-d', default='data',
                        help='Директория для сохранения результатов работы')
    parser_parse.add_argument('-start', default=None,
                              help='Начать заполнение очереди с указанного элемента')
    parser_parse.add_argument('-stop', default=None,
                              help='Закончить заполнение очереди на указанном элемете')
    parser_parse.add_argument('-r', choices=SET_OF_RESOURCES, default=HHRU, type=int,
                              help='Выбор ресурса для обработки')
    parser_parse.set_defaults(func=parse_list)

    return parser.parse_args()


def main():
    """Это все, что нам потребуется для обработки всех ветвей аргументов"""
    args = parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
